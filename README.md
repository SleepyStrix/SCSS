# Silent's Cel Shading Shaders
Shaders for Unity for cel shading, based off CubedParadox's Flat Lit Toon, feating a number of handy features.
# [Want to know how to use this shader? Here's the manual!](https://gitlab.com/s-ilent/SCSS/wikis/Manual/Setting-Overview)
Features include:
* Customisable lighting through light ramp textures
* Specular and smoothness functionality similar to Unity's Standard shader
* Shadow mask texture for painting areas of shadow or light bias, for ambient occlusion and/or keeping areas lit more consistently without having to change vertex normals
* Customisable ambient fresnel and emissive fresnel for shiny effects
* Customisable additive and multiplicative matcaps, anchored in world-space so they won't sway when you turn your head
* Lots and lots of advanced options for blend mode and more
* A render queue slider (note: VRchat currently overrides custom render queues set this way)
* Cleaner outlines that reduce size based on camera proximity
* Compatible with texture types used in MMD models

<div style="width: 5em">![Too Much Preview](https://cdn.discordapp.com/attachments/445750247147438083/505926650781761561/2018-10-28_12-41-31.jpg)</div>

# [For more details, please check the setting overview!](https://gitlab.com/s-ilent/SCSS/wikis/Manual/Setting-Overview)

Tested with Unity 5.6.3p1 and Unity 2017.4.15f1 LTS.

For support, contact me on Discord.
![Silent＃0264](https://files.catbox.moe/lv2mdh.png)